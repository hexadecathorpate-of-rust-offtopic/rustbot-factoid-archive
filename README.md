# rustbot factoids archive

This repository contains a copy of the database of "factoids" that was used by
[Havvy]'s IRC bot "rustbot", before said bot went offline in mid-2019,
apparently having been replaced by [dgriffen]'s "rustybot", whose factoid
database is not publicly available at this time.

This copy of the rustbot factoid database was copied from
<https://havvy.net/rustbot/factoids.db> on 2019-06-12 UTC.

[Havvy]: <https://github.com/Havvy>
[dgriffen]: <https://github.com/dgriffen>
